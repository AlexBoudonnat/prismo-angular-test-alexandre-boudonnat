import { Component, OnInit } from '@angular/core';
import {MemberService} from "../services/member.service";
import {Member} from "../member";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-members-list',
  templateUrl: './members-list.component.html',
  styleUrls: ['./members-list.component.scss']
})
export class MembersListComponent implements OnInit {

  members: Member[];
  membersFromApi: Member[];

  constructor(private memberService: MemberService, private apiService: ApiService) { }

  ngOnInit() {
    //Récupère les membres depuis le json en dur (Old : peut etre supprimé)
    this.members = this.memberService.member;

    // Récupère tous les membres depuis le serveur node.js grace à une API
    this.apiService.getAllMembers().subscribe((res)=>{
      this.membersFromApi = res.data;
    });
  }

}
