import {Component, OnDestroy, OnInit} from '@angular/core';
import {MemberService} from "../services/member.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {ApiService} from "../services/api.service";
import {Member} from "../member";

@Component({
  selector: 'app-member-profil',
  templateUrl: './member-profil.component.html',
  styleUrls: ['./member-profil.component.scss']
})
export class MemberProfilComponent implements OnInit, OnDestroy {

  member: any;
  selectedMember: Member;
  private routeSub: Subscription;

  constructor(private memberService: MemberService,private apiService: ApiService , private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    //Récupère le membre selectionné dans la BDD grâce à l'id de l'url
    this.getSelectedMember();

    //Récupère le membre selectionné dans la liste (Old Method : peut etre supprimé)
    this.memberService.currentMember.subscribe(member => {
      this.member = member;
      // Si aucun membre selectionné, retour à la liste
      // if (!this.member.id) {
      //   this.router.navigateByUrl('/members-list');
      // }
    });

  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  // Récupère le membre selectionné en BDD grace au service api
  getSelectedMember() {
    this.routeSub = this.route.params.subscribe(params => {
      this.apiService.getMemberById(params['id']).subscribe( res => {
        this.selectedMember = res.data[0];
        //Envoi le membre au member service pour qu'il le stock dans l'historique
        this.memberService.addVisitedMember(this.selectedMember);
      });
    });
  }

}
