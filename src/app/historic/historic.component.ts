import { Component, OnInit } from '@angular/core';
import {Member} from "../member";
import {MemberService} from "../services/member.service";

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.scss']
})
export class HistoricComponent implements OnInit {

  visitedMembersList = [];

  constructor(private memberService: MemberService) { }

  ngOnInit() {
    //Récupère la liste des profils deja visités
    this.visitedMembersList = this.memberService.visitedMembersList;
  }

}
