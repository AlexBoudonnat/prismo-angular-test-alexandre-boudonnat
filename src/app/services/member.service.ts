import {BehaviorSubject} from "rxjs";
import * as membersData from "../../assets/datas/members.json";
import {Injectable} from "@angular/core";
import {Member} from "../member";

@Injectable()
export class MemberService {

  // Récuperation des datas de members.json
  member:Array<Member> = membersData.default;

  // Initialisation pour récuperation des datas d'un membre choisi
  private memberSource = new BehaviorSubject(Member);
  currentMember = this.memberSource.asObservable();
  visitedMembersList = [];

  newMember = new Member();

  constructor() { }

  // Récupère le membre selectionné pour le passer entre les components
  changeMember(member: any) {
    this.memberSource.next(member);
  }

  //Ajout un membre à l'historique quand son profil est visité
  addVisitedMember(member: any) {
    this.visitedMembersList.push(member);
  }

  //Rajoute un nouveau membre à la liste de membres
  addMember(member: Member) {
    this.newMember = member;
    this.newMember.id = this.member.length + 1;
    this.member.push(this.newMember);
  }
}
