import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Member} from "../member";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiURL: string = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  getAllMembers() {
    return this.httpClient.get<any>(this.apiURL+'/members');
  }

  getMemberById(id: string) {
    return this.httpClient.get<any>(this.apiURL + '/member/' + id);
  }

  addMember(member: Member) {
    return this.httpClient.post<Member>(this.apiURL + '/member', member, httpOptions);
  }
}
