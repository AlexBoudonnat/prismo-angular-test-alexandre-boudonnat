import {Component, Input, OnInit} from '@angular/core';
import {MemberService} from "../services/member.service";
import {Router} from "@angular/router";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss'],
  animations: [
    trigger('turningAvatar',  [
      state('normal', style({transform: 'rotate(0deg)'})),
      state('goneWild', style({transform: 'rotate(180deg)'})),
      transition('normal => goneWild', [
        animate('.3s')
      ]),
      transition('goneWild => normal', [
        animate('.3s')
      ]),
    ]),
  ]
})
export class MemberCardComponent implements OnInit {

  @Input() member: any;

  isNormal = true;

  constructor(private memberService: MemberService, private router: Router) {
    this.getAnim();
  }

  ngOnInit() {
  }

  // Passe le membre choisi au service puis navigue vers le profil de ce membre
  getMemberProfil(member: any) {
    this.memberService.changeMember(member);
    this.router.navigateByUrl('/member/' + member.id);
  }

  getAnim() {
    setTimeout(
      () => {
        this.isNormal = !this.isNormal;
        this.getAnim();
      }, 4000
    );
  }

}
