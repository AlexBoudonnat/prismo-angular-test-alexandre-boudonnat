import {Component, Input, OnInit} from '@angular/core';
import {Member} from "../member";
import {MemberService} from "../services/member.service";
import {Router} from "@angular/router";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Input() firstname: string;
  @Input() lastname: string;
  @Input() avatar: string;
  @Input() function: string;
  @Input() description: string;

  newMember = new Member();

  constructor(private memberService: MemberService, private apiService: ApiService, private router: Router) { }

  ngOnInit() {
  }

  addMember() {
    //On rentre les données du formulaire dans un nouvel objet Member
    this.newMember.firstname = this.firstname;
    this.newMember.lastname = this.lastname;
    this.newMember.avatar = this.avatar;
    this.newMember.function = this.function;
    this.newMember.description = this.description;

    //On envoie le nouveau membre au service pour qu'il utilise l'API pour l'insérer en BDD
    this.apiService.addMember(this.newMember).subscribe(() => this.router.navigateByUrl('/members-list'));

    //On envoie le newMember au service pour qu'il le rajoute à la liste de membre (Old : à supprimer)
    //Puis on redirige vers la liste des membres (Old : à supprimer)
    this.memberService.addMember(this.newMember);
    // this.router.navigateByUrl('/members-list');
  }

}
