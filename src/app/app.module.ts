import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HeaderComponent } from './header/header.component';
import { FourZeroFourComponent } from './four-zero-four/four-zero-four.component';
import { MembersListComponent } from './members-list/members-list.component';
import { MemberCardComponent } from './member-card/member-card.component';
import { MemberProfilComponent } from './member-profil/member-profil.component';
import { FormComponent } from './form/form.component';

import { MemberService } from "./services/member.service";
import { ApiService } from "./services/api.service";
import { HistoricComponent } from './historic/historic.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    FourZeroFourComponent,
    MembersListComponent,
    MemberCardComponent,
    MemberProfilComponent,
    FormComponent,
    HistoricComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: WelcomeComponent },
      { path: 'welcome', component: WelcomeComponent },
      { path: 'members-list', component: MembersListComponent },
      { path: 'member/:id', component: MemberProfilComponent },
      { path: 'create-member', component: FormComponent },
      { path: 'historic', component: HistoricComponent },
      { path: 'not-found', component: FourZeroFourComponent },
      { path: '**', redirectTo: 'not-found' }
    ])
  ],
  providers: [
    MemberService,
    ApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
